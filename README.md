# Terra Devnet on MANTRA DAO

- Chain ID: `pisco-1`

## Submodules

- [terra-money/core](https://github.com/terra-money/core.git) @ `v2.1.0-beta.1`
- [terra-money/oracle-feeder](https://github.com/terra-money/oracle-feeder) @ `v2.0.2`
